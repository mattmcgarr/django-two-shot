from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.db.models import Count

# Create your views here.


@login_required
def receipt(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt": receipt}
    return render(request, "receipts/receipts.html", context)


@login_required
def category_list(request):
    expense = ExpenseCategory.objects.filter(owner=request.user).annotate(
        count=Count("receipts")
    )
    context = {"expense": expense}
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user).annotate(
        count=Count("receipts")
    )
    context = {"account": account}
    return render(request, "receipts/accounts.html", context)


@login_required
def create_receipt(request):
    form = ReceiptForm()
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
        else:
            form = ReceiptForm()
    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required
def create_category(request):
    form = ExpenseCategoryForm()
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
        else:
            form = ExpenseCategoryForm()
    context = {"form": form}
    return render(request, "receipts/categorycreate.html", context)


@login_required
def create_account(request):
    form = AccountForm()
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
        else:
            form = AccountForm()
    context = {"form": form}
    return render(request, "receipts/accountscreate.html", context)
